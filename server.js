'use strict'
// Express
var express = require('express');
var globals = require('./package.json');

//webpack
var webpack = require('webpack');
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');
var config = require('./webpack.config');

var proxy = require('express-http-proxy');



var app = express();
var compiler = webpack(config);

var httpserver = 'http://localhost:3001';

app.use(webpackDevMiddleware(compiler, {
    'noInfo': true,
    'publicPath': config.output.publicPath
  }));

app.use(webpackHotMiddleware(compiler, {
    'log': console.log, //false
    'path': '/__webpack_hmr',
    //path: config.output.publicPath + '__webpack_hmr'
    'heartbeat': 10 * 1000
}));


app
	.get('/',(req, res) => {
		console.log( "URL", req.url);
		res.sendFile(__dirname + '/build/index.html');
	})

  .use('/build', express.static('build'))
	.use('/static', express.static('static'))
	.use('/node_modules',express.static('node_modules'))
	.use('/partials',express.static('partials'))
	.use('/carelogic', proxy(httpserver, {
		'forwardPath': function (req, res) {
			console.log('carelogic forward ' + req.url);

			return '/carelogic' + req.url;
		}
	}))
	.listen(globals.config.port, function (error) {
		if (error) {
			console.error(error);
		} else {
			console.info('==> Listening on port %s. Open up http://localhost:%s/ in your browser.', globals.config.port, globals.config.port);
		}
	});
