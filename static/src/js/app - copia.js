var myApp = angular.module("myApp", ['ui.router']);
//var myApp = angular.module("myApp", ["ngRoute"]);
//myApp.config(function($routeProvider) {
myApp.config(function($stateProvider, $urlRouterProvider){	
		$urlRouterProvider.otherwise("/books");
		
		$stateProvider
		.state('StateMenuBooks', {
			url:"/books",
			templateUrl:"partials/book-list.html",
			controller: "BookListCtrl"
			}) 
		.state('StateMenuKart', {
				url:"/kart",
				templateUrl: "partials/kart-list.html",
				controller: "KartListCtrl"
		});	

});

myApp.factory("kartService", function() {
	var kart = [];
	
	return {
		getKart: function() {
			return kart;
		},
		addToKart: function(book) {
			kart.push(book);
		},
		buy: function(book) {
			alert("Thanks for buying: " + book.name);
		}
	}
});

myApp.service("bookService", function($http) {
	
	this.getBooks = function (){
		var books = $http.get('static/data/data.json').then(function (response) {
          console.log(response);
          return response.data;
        });
		return books;
	}
	this.addToKart = function (book){
		
	}	

});

/* 
myApp.factory("bookService", function($http) {
	var books = $http.get('static/data/data.json').then(function (response) {
          console.log(response);
          return response.data;
        });
		
	return {
		getBooks: function() {
			return books;
		},
		addToKart: function(book) {
			
		}
	}
}); */

myApp.controller("KartListCtrl", function($scope, kartService) {
	$scope.kart = kartService.getKart();
	
	$scope.buy = function(book) {
		//console.log("book: ", book);
		kartService.buy(book);
	}
});

myApp.controller("HeaderCtrl", function($scope, $location) {
	$scope.appDetails = {};
	$scope.appDetails.title = "BooKart";
	$scope.appDetails.tagline = "We have collection of 1 Million books";
	
	$scope.nav = {};
	$scope.nav.isActive = function(path) {
		if (path === $location.path()) {
			return true;
		}
		
		return false;
	}
});

myApp.controller("BookListCtrl", function($scope, bookService, kartService) {
	 bookService.getBooks().then(function(books){
		 console.log(books)
		 $scope.books = books;
	 })
	
	$scope.addToKart = function(book) {
		kartService.addToKart(book);
	}
});