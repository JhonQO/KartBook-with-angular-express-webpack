
var myApp = angular.module("StoreControllerModule",[]);

myApp.controller("KartListCtrl", function($scope, kartService) {
	$scope.kart = kartService.getKart();

	$scope.buy = function(book) {
		//console.log("book: ", book);
		kartService.buy(book);
	}
});

myApp.controller("HeaderCtrl", function($scope, $location) {
	$scope.appDetails = {};
	$scope.appDetails.title = "BooKart";
	$scope.appDetails.tagline = "We have collection of 1 Million books";

	$scope.nav = {};
	$scope.nav.isActive = function(path) {
		if (path === $location.path()) {
			return true;
		}

		return false;
	}
});

myApp.controller("BookListCtrl", function($scope, bookService, kartService) {
	 bookService.getBooks().then(function(books){
		 console.log(books)
		 $scope.books = books;
	 })

	$scope.addToKart = function(book) {
		kartService.addToKart(book);
	}
});
