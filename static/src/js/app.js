
require('angular-ui-router')
require('angular-animate')

require('./services/storeService.js')
require('./controllers/storeController.js')
import angular from 'angular';

var myApp = angular.module("myApp", [
															'ui.router',
															'ngAnimate',
															'StoreControllerModule',
														 	'StoreServiceModule']);

//var myApp = angular.module("myApp", ["ngRoute"]);
//myApp.config(function($routeProvider) {
myApp.config(function($stateProvider, $urlRouterProvider){
		$urlRouterProvider.otherwise("/books");

		$stateProvider
		.state('StateMenuBooks', {
			url:"/books",
			templateUrl:"partials/book-list.html",
			controller: "BookListCtrl"
			})
		.state('StateMenuKart', {
				url:"/kart",
				templateUrl: "partials/kart-list.html",
				controller: "KartListCtrl"
		});

});
