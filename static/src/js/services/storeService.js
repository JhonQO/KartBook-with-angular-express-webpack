var myApp = angular.module("StoreServiceModule",[]);

myApp.factory("kartService", function() {
	var kart = [];
	
	return {
		getKart: function() {
			return kart;
		},
		addToKart: function(book) {
			kart.push(book);
		},
		buy: function(book) {
			alert("Thanks for buying: " + book.name);
		}
	}
});

myApp.service("bookService", function($http) {

	this.getBooks = function (){
		var books = $http.get('static/data/data.json').then(function (response) {
          console.log(response);
          return response.data;
        });
		return books;
	}
	this.addToKart = function (book){

	}

});

/*
myApp.factory("bookService", function($http) {
	var books = $http.get('static/data/data.json').then(function (response) {
          console.log(response);
          return response.data;
        });

	return {
		getBooks: function() {
			return books;
		},
		addToKart: function(book) {

		}
	}
}); */
