var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack')
var path = require('path')
var globals = require('./package.json');

module.exports = {
  entry: {
    main: [path.join(__dirname,'static', 'src', globals.config.entryPath, 'app.js'),
          'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true'
          ]
  },
  output: {
    path: globals.config.outputPath,
    filename: "scrips.min.js"
  },
  module:{
    loaders: [
      { test: /\.js$/, loader: 'babel-loader',
        exclude: /node_module/, query: { presets: ["es2015"]}
      },
    ]
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
]
}
